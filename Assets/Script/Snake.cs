﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    private Vector2Int gridPosition;
    private Vector2Int snakePosition;
    private direction gridMoveSnake;
    private float movementSpeed;
    private float maxMovementSpeed;
    private LevelGrid levelGrid;
    private GameObject snakeBody;
    private List<GameObject> snakeBodyList;
    private bool isBodyAdded;
    
public void setup(LevelGrid levelGrid){
    this.levelGrid=levelGrid;
}
 private enum direction{
        up,
        down,
        left,
        right
    }
    
    // Start is called before the first frame update
    private void Start(){
        gridPosition = new Vector2Int(0,0); 
    }
 private void Awake()
 {
        gridPosition = new Vector2Int(0,0); 
        gridMoveSnake = direction.up;  
        maxMovementSpeed=.2f;
        movementSpeed=maxMovementSpeed; 
        isBodyAdded = false;
        snakeBodyList = new List<GameObject>();
 }   
   private void Update()
    { //To do -Change direction on keypress 
        handleSnakeMovement();   
        changeSnakePosition();   
        transform.position=new Vector3(gridPosition.x,gridPosition.y);               
    }
public void handleSnakeMovement(){
    
        movementSpeed += Time.deltaTime;
        if(movementSpeed >= maxMovementSpeed){
            movementSpeed -= maxMovementSpeed;
            Vector2Int gridMoveDirectionVector; 
            gridMoveDirectionVector=new Vector2Int();

            if(gridMoveSnake==direction.up){
                gridMoveDirectionVector=new Vector2Int(0,1);
            }else if(gridMoveSnake==direction.down){
                gridMoveDirectionVector=new Vector2Int(0,-1);
            }else if(gridMoveSnake==direction.left){
                gridMoveDirectionVector=new Vector2Int(-1,0);
            }else if (gridMoveSnake==direction.right){
                gridMoveDirectionVector=new Vector2Int(1,0);
            }
             gridPosition += gridMoveDirectionVector;  
             gridPosition=levelGrid.resetSnakeGridPosition(gridPosition); 
    }
}
private void OnTriggerEnter2D(Collider2D other)
{ 
    levelGrid.snakeEats();
   isBodyAdded = addSnakeBody();
}
public Vector2Int getGridPosition(){
    return gridPosition;
}
public bool addSnakeBody(){
   snakeBody = GameObject.Find("SnakeBody");
   snakeBodyList.Add(snakeBody);

   if (snakeBodyList.Count<=1){
       Rigidbody2D snakeBodyRb = snakeBody.GetComponent<Rigidbody2D>();
       FixedJoint2D snakeBodyFj = transform.GetComponent<FixedJoint2D>();
       snakeBodyFj.connectedBody = snakeBodyRb;
   }
   else{
       int cnt=0;
       foreach (GameObject snakeBody in snakeBodyList)
       {
           if (cnt%2==1 && snakeBody.GetComponent<FixedJoint2D>()==null)
           {
               GameObject prevSnakeBody = snakeBodyList[cnt-1];
               Rigidbody2D prevRigidbody=prevSnakeBody.GetComponent<Rigidbody2D>();
               FixedJoint2D snakeBodyFj = snakeBody.AddComponent<FixedJoint2D>();
               snakeBodyFj.autoConfigureConnectedAnchor=false;
               snakeBodyFj.connectedAnchor=new Vector2Int(0,0); 
               snakeBodyFj.enableCollision=true;
               snakeBodyFj.connectedBody = prevRigidbody;
           } 
           cnt++;
       }
   }
   snakeBody.transform.position=new Vector3(transform.position.x,transform.position.y);
   // add a list of objects and attach to fixed joint
    return true;
}
private void changeSnakePosition(){
     if(Input.GetKeyDown(KeyCode.UpArrow)){
         if(gridMoveSnake!=direction.down){
            gridMoveSnake=direction.up;
            transform.rotation = Quaternion.Euler(0, 0,0);
            
         }
    }
        if(Input.GetKeyDown(KeyCode.DownArrow)){
            if(gridMoveSnake!=direction.up){
                gridMoveSnake=direction.down;   
                transform.rotation = Quaternion.Euler(0, 0, 180);
            }
        }
        if(Input.GetKeyDown(KeyCode.LeftArrow)){
            if(gridMoveSnake!=direction.right){
                gridMoveSnake=direction.left;
                transform.rotation = Quaternion.Euler(0, 0, 90);
            }
        }
        if(Input.GetKeyDown(KeyCode.RightArrow)){
            if(gridMoveSnake!=direction.left){
                gridMoveSnake=direction.right;
                transform.rotation = Quaternion.Euler(0, 0, 270);
            }
        }
       if (isBodyAdded){
           snakeBody.transform.rotation = transform.rotation;
       } 
}


}
