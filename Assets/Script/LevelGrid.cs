﻿using System.Diagnostics;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelGrid : MonoBehaviour
{
    private Snake snake;
    private int width;
    private int height;
    private GameObject foodGameObject;
    private Vector2Int foodGridPosition;

    // Start is called before the first frame update
public LevelGrid (int width, int height){
    this.width=width;
    this.height=height;
    spawnFood();
}

public void setup(Snake snake){
    this.snake=snake;
}

private void spawnFood(){
    foodGridPosition = new Vector2Int(Random.Range(-33, 32),Random.Range(-18, 18));
    foodGameObject= GameObject.Find("SnakeFood");
    foodGameObject.transform.position=new Vector3(foodGridPosition.x,foodGridPosition.y);
}

public void snakeEats(){
        spawnFood();   
}

public Vector2Int resetSnakeGridPosition(Vector2Int gridPosition){
    if(gridPosition.y==height){
        gridPosition.y=1-height;
    }
    if(gridPosition.y==-height){
        gridPosition.y=height-1;
    }
    if(gridPosition.x==width){
        gridPosition.x=1-width;
    }
    if(gridPosition.x==-width){
        gridPosition.x=width-1;
    }
    return gridPosition;
 }
}
