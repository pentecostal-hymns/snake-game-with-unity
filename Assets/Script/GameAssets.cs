﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAssets : MonoBehaviour
{
    public static GameAssets gameAsset;
    // Start is called before the first frame update

    private void Awake()
    {
        gameAsset=this;
    }

    public Sprite snakeHead;
    public Sprite snakeFood;
    public Sprite snakeBody;
}
