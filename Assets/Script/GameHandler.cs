﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CodeMonkey;
using CodeMonkey.Utils;

public class GameHandler : MonoBehaviour
{
  private GameHandler instance;
  [SerializeField]private Snake snake;
  private LevelGrid levelGrid;
       // Start is called before the first frame update
  private void Awake()
  {
    instance=this;
    
  }
  private void Start(){
    levelGrid=new LevelGrid(35,20);
    snake.setup(levelGrid);
    levelGrid.setup(snake);
  }
    // Update is called once per frame
}
